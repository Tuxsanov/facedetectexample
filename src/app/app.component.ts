import { Component, AfterContentInit } from '@angular/core';
import * as faceapi from 'face-api.js';

// implements nodejs wrappers for HTMLCanvasElement, HTMLImageElement, ImageData
// import * as canvas from 'canvas';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterContentInit{
  title = 'fAPI';

  
  constructor(){
  }
  
  ngAfterContentInit(){
    const input:any = document.getElementById('myImg');
    async function f1() {
      // await faceapi.nets.ssdMobilenetv1.loadFromUri('/models')
      await faceapi.loadSsdMobilenetv1Model('/../assets/models');
      await faceapi.loadTinyFaceDetectorModel('/../assets/models')
      await faceapi.loadMtcnnModel('/../assets/models')
      await faceapi.loadFaceLandmarkModel('/../assets/models')
      await faceapi.loadFaceLandmarkTinyModel('/../assets/models')
      await faceapi.loadFaceRecognitionModel('/../assets/models')
      const results = await faceapi.detectAllFaces(input)
        .withFaceLandmarks()
        .withFaceDescriptors()
  
      if (!results.length) {
        return
      }

      // const faceMatcher = new faceapi.FaceMatcher(results);

      const detections = await faceapi.detectAllFaces(input)

      // resize the detected boxes in case your displayed image has a different size then the original
      const detectionsForSize = faceapi.resizeResults(detections, { width: input.width, height: input.height })
      // draw them into a canvas
      const canvas:any = document.getElementById('overlay');
      canvas.width = input.width;
      canvas.height = input.height;
      // faceapi.drawDetection(canvas, detectionsForSize, { withScore: true })
      // let fullFaceDescriptions = await faceapi.detectAllFaces(input).withFaceLandmarks().withFaceDescriptors();
      const landmarksArray = results.map(fd => fd.landmarks)
      faceapi.drawLandmarks(canvas, landmarksArray, { drawLines: true });

      const labels = ['remon'];
      const labeledFaceDescriptors = await Promise.all(
        labels.map(async label => {
          // fetch image data from urls and convert blob to HTMLImage element
          const imgUrl = `./assets/${label}.PNG`
          const img = await faceapi.fetchImage(imgUrl)
          
          // detect the face with the highest score in the image and compute it's landmarks and face descriptor
          const fullFaceDescription = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor()
          
          if (!fullFaceDescription) {
            throw new Error(`no faces detected for ${label}`)
          }
          
          const faceDescriptors = [fullFaceDescription.descriptor]
          return new faceapi.LabeledFaceDescriptors(label, faceDescriptors)
        })
      )

      const maxDescriptorDistance = 0.6;
      const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, maxDescriptorDistance);

      const _results = results.map(fd => faceMatcher.findBestMatch(fd.descriptor))
    
      const boxesWithText = _results.map((bestMatch, i) => {
        const box = results[i].detection.box
        const text = bestMatch.toString()
        const boxWithText = new faceapi.BoxWithText(box, text)
        return boxWithText
      })
      
      faceapi.drawDetection(canvas, boxesWithText)
    }
    f1();
  }
}
